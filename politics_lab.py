voting_data = list(open("voting_record_dump109.txt"))

## Task 1

def create_voting_dict():
    """
    Input: None (use voting_data above)
    Output: A dictionary that maps the last name of a senator
            to a list of numbers representing the senator's voting
            record.
    Example: 
        >>> create_voting_dict()['Clinton']
        [-1, 1, 1, 1, 0, 0, -1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, -1, 1, 1, 1, 1, -1, 1, 1, 1]

    This procedure should return a dictionary that maps the last name
    of a senator to a list of numbers representing that senator's
    voting record, using the list of strings from the dump file (strlist). You
    will need to use the built-in procedure int() to convert a string
    representation of an integer (e.g. '1') to the actual integer
    (e.g. 1).

    You can use the split() procedure to split each line of the
    strlist into a list; the first element of the list will be the senator's
    name, the second will be his/her party affiliation (R or D), the
    third will be his/her home state, and the remaining elements of
    the list will be that senator's voting record on a collection of bills.
    A "1" represents a 'yea' vote, a "-1" a 'nay', and a "0" an abstention.

    The lists for each senator should preserve the order listed in voting data. 
    """

    with open('voting_record_dump109.txt') as f:
        l = list(f)
    l=[a.split() for a in l]
    return {a[0]: [int(b) for b in a[3:]] for a in l}

## Task 2

def policy_compare(sen_a, sen_b, voting_dict):
    """
    Input: last names of sen_a and sen_b, and a voting dictionary mapping senator
           names to lists representing their voting records.
    Output: the dot-product (as a number) representing the degree of similarity
            between two senators' voting policies
    Example:
        >>> voting_dict = {'Fox-Epstein':[-1,-1,-1,1],'Ravella':[1,1,1,1]}
        >>> policy_compare('Fox-Epstein','Ravella', voting_dict)
        -2
    """
    assert sen_a in voting_dict
    assert sen_b in voting_dict
    return sum(map(lambda x,y:x*y, voting_dict[sen_a], voting_dict[sen_b]))


## Task 3

def most_similar(sen, voting_dict):
    """
    Input: the last name of a senator, and a dictionary mapping senator names
           to lists representing their voting records.
    Output: the last name of the senator whose political mindset is most
            like the input senator (excluding, of course, the input senator
            him/herself). Resolve ties arbitrarily.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> most_similar('Klein', vd)
        'Fox-Epstein'

    Note that you can (and are encouraged to) re-use you policy_compare procedure.
    """
    assert sen in voting_dict
    sim = ""
    score = float('-inf')
    for sen_b in voting_dict:
        if sen != sen_b:
            new_score = policy_compare(sen, sen_b, voting_dict)
            if new_score >= score:
                score = new_score
                sim = sen_b
    return sim

## Task 4

def least_similar(sen, voting_dict):
    """
    Input: the last name of a senator, and a dictionary mapping senator names
           to lists representing their voting records.
    Output: the last name of the senator whose political mindset is least like the input
            senator.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> least_similar('Klein', vd)
        'Ravella'
    """
    assert sen in voting_dict
    sim = ""
    score = float('inf')
    for sen_b in voting_dict:
        if sen != sen_b:
            new_score = policy_compare(sen, sen_b, voting_dict)
            if new_score <= score:
                score = new_score
                sim = sen_b
    return sim

## Task 5

vd = create_voting_dict()
most_like_chafee    = most_similar('Chafee', vd)
least_like_santorum = least_similar('Santorum', vd)

# Task 6

def find_average_similarity(sen, sen_set, voting_dict):
    """
    Input: the name of a senator, a set of senator names, and a voting dictionary.
    Output: the average dot-product between sen and those in sen_set.
    Example:
        >>> vd = {'Klein': [1,1,1], 'Fox-Epstein': [1,-1,0], 'Ravella': [-1,0,0]}
        >>> find_average_similarity('Klein', {'Fox-Epstein','Ravella'}, vd)
        -0.5
    """
    sen_set = sen_set.copy()
    sen_set.discard(sen)
    n = len(sen_set)
    return sum([policy_compare(sen, b, voting_dict) for b in sen_set if b in
        voting_dict])/n

def getDemocrats():
    ''' This function retrieves a set of all the democrates from the voting 
    records file '''
    with open('voting_record_dump109.txt') as f:
        l = list(f)
    l = [a.split() for a in l]
    return { a[0] for a in l if a[1]=='D' }


avdict = {sen: find_average_similarity(sen, set(vd.keys()), vd) for sen in
            getDemocrats()}
most_average_democrat = max(avdict, key=lambda x:avdict[x])
# give the last name (or code that computes the last name)


# Task 7

def find_average_record(sen_set, voting_dict):
    """
    Input: a set of last names, a voting dictionary
    Output: a vector containing the average components of the voting records
            of the senators in the input set
    Example: 
        >>> voting_dict = {'Klein': [-1,0,1], 'Fox-Epstein': [-1,-1,-1], 'Ravella': u0,0,1]}
        >>> find_average_record({'Fox-Epstein','Ravella'}, voting_dict)
        [-0.5, -0.5, 0.0]
    """
    sen_set = { s for s in sen_set if s in voting_dict }
    nvotes = len(voting_dict[ list(sen_set)[0] ])
    nsen = len(sen_set)
    from functools import reduce
    return list(
                map( lambda x:x/nsen,
                    reduce( lambda l1, l2:[l1[i]+l2[i] for i in range(nvotes)],
                        [voting_dict[k] for k in sen_set],
                        [0,]*nvotes
                    )
                )
            )

average_Democrat_record = find_average_record(getDemocrats(), vd)  # (give the vector)


# Task 8

def bitter_rivals(voting_dict):
    """
    Input: a dictionary mapping senator names to lists representing
           their voting records
    Output: a tuple containing the two senators who most strongly
            disagree with one another.
    Example: 
        >>> voting_dict = {'Klein': [-1,0,1], 'Fox-Epstein': [-1,-1,-1], 'Ravella': [0,0,1]}
        >>> bitter_rivals(voting_dict)
        ('Fox-Epstein', 'Ravella')
    """
    sen_set = set( voting_dict.keys() )
    lsdict = { sen: least_similar(sen, voting_dict) for sen in sen_set }
    lscmp  = { sen: policy_compare(sen, lsdict[sen], voting_dict) for sen in sen_set }
    r1 = min(lscmp, key=lambda x:lscmp[x])
    return (r1, lsdict[r1])

